# gscplatform.io Smart Contracts

## ICO details...

There will be several stages of the token sale, each defined with the amount of tokens on offer and the associated bonus. 

All tokens that are not sold in each stage will be burned.

Token sale will have a hardcap and a softcap. 

## Token details

GSCP will be an ERC20 Ethereum token.

## About gscplatform.io Project

Here are the links:

* [Product website](https://gscplatform.io/)
* [ICO website](https://gscplatform.io/)
* [Telegram](https://t.me/GSCAviationOfficial)
* 

## SmartContract Details

GSCP smartcontract is live on Ethereum Main network and you can see more info at 

[GSCP](https://etherscan.io/token/0x186e20C062529d65bFf63bf4aDB56665923D4DD2)

Also any payment for this contract will be lost. Please pay using our dashboard only

We are not responsible if you pay anything to this address